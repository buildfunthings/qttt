Quantum Tic Tac Toe
===

[A game](https://en.wikipedia.org/wiki/Quantum_tic-tac-toe) that combines quantum mechanics with tic tac toe, what else can be better?

In this series we will be building the game and all the required infrastructure to host the game for multi player madness.

Instructions
---

To work on the project I recommend using the Emacs editor together with Cider. You can just `cider-jack-in-clojurescript` and the environment works like magic. The project is fully self-contained at the moment and does not require external dependencies.

Contributing
---

If you want to contribute I highly encourage you to join the Friday 10am CET live episodes where Martin and I walk through the project step by step.

Pull requests and Issue reporting is highly encouraged!
