(ns qttt.views.board-test
  (:require [clojure.test :as t]
            [qttt.views.board :as sut]
            [reagent.core :as r]))

(def rflush r/flush)

(defn add-test-div [name]
  (let [doc js/document
        body (.-body js/document)
        div (.createElement doc "div")]
    (.appendChild body div)
    div))

(defn with-mounted-component [comp f]
  (let [div (add-test-div "_testreagent")]
    (let [c (r/render-component comp div)]
      (f c div)
      (r/unmount-component-at-node div)
      (r/flush)
      (.removeChild (.-body js/document) div))))

(defn found-in [re div]
  (let [res (.-innerHTML div)]
    (if (re-find re res)
      true
      (do (println "Not found: " res)
          false))))


(t/deftest cell-superposition
  ;; Ensure that a superposition is 10 fields, the container and 9 subcells
  (with-mounted-component [(fn []
                             [:table [:tbody [:tr (sut/create-cell {:type :superposition :cell 1})]]]) nil nil]
    (fn [rendered div]
      (t/is (= 10 (count (re-seq #"<td" (.-innerHTML div)))))
      )
    ))
        
(t/deftest cell-open-cell
  ;; Open cell now shows index of cell
  (with-mounted-component [(fn []
                             [:table [:tbody [:tr (sut/create-cell {:type :open :cell 1})]]]) nil nil]
    (fn [rendered div]
      (t/is (= true  (found-in #"open" div)))
      )
    ))

(t/deftest cell-classical-cell
  ;; Open cell now shows index of cell
  (with-mounted-component [(fn []
                             [:table [:tbody (sut/create-cell {:type :classical :cell 1})]]) nil nil]
    (fn [rendered div]
      (t/is (= true  (found-in #"span" div))) ;; TODO fix testcase, span is nonsense
      )
    ))
