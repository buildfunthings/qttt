(ns qttt.runner
  (:require  [clojure.test :as t]
             [doo.runner :refer-macros [doo-tests]]

             qttt.views.board-test 
             qttt.events.board-test
             qttt.game-test))

(enable-console-print!)

(doo-tests 'qttt.views.board-test
           'qttt.events.board-test
           'qttt.game-test)
