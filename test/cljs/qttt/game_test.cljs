(ns qttt.game-test
  (:require [qttt.game :as sut]
            [cljs.test :as t :include-macros true]))


;; Example board, paths
;; [1 3] [0 5] [0 2] [1 0]
;; [0 2] [1 0] [1 3] [0 5]
;; [2 1] [0 3] [0 2] [1 0] [1 3] [0 5] -- failed, all siblings in path
;; (def board {0 {2 {:toggle true, :turn 0, :player 0, :entangle [1 0]},
;;                3 {:toggle true, :turn 2, :player 1, :entangle [2 1]}
;;                5 {:toggle true, :turn 1, :player 1, :entangle [1 3]}},
;;             1 {0 {:toggle true, :turn 0, :player 0, :entangle [0 2]},
;;                3 {:toggle true, :turn 1, :player 1, :entangle [0 5]}},
;;             2 {1 {:toggle true, :turn 2, :player 1, :entangle [0 3]}}})

(def board {0 {:entanglements
               {2 {:toggle true, :turn 0, :player 0, :entangle [1 0]},
                3 {:toggle true, :turn 2, :player 0, :entangle [2 0]},
                5 {:toggle true, :turn 1, :player 1, :entangle [1 3]}}},
            1 {:entanglements
               {0 {:toggle true, :turn 0, :player 0, :entangle [0 2]},
                3 {:toggle true, :turn 1, :player 1, :entangle [0 5]}}},
            2 {:entanglements
               {0 {:toggle true, :turn 2, :player 0, :entangle [0 3]}}}})


(defn get-cell [board cell]
  (get-in board cell))

(t/deftest path-helpers
  (= (sut/path-contains-cell? [[0 1] [3 4] [5 6]] [0 1]) true)
  (= (sut/path-contains-cell? [[0 1] [3 4] [5 6]] [3 5]) nil)
  (= (sut/path-visited-cell? [[0 1] [3 4] [5 6]] 3) true)
  (= (sut/path-visited-cell? [[0 1] [3 4] [5 6]] 6) false)
  )

(t/deftest single-other-cell
  (= (sut/get-other-cells board [1 3]) (list [0 {:toggle true, :turn 0, :player 0, :entangle [0 2]}]))
  (= (sut/get-other-cells board [0 2]) (list [3 {:toggle true, :turn 2, :player 1, :entangle [2 1]}]
                                             [5 {:toggle true, :turn 1, :player 1, :entangle [1 3]}]))
  )


(t/deftest cycle-find
  (t/is (=  (sut/find-cycles-dfs board [1 3])
            (list [1 3] [0 5] [0 2] [1 0])))
  (t/is (=  (sut/find-cycles-dfs board [2 1]) (list )))
  (t/is (=  (sut/find-cycles-dfs board [0 2])
            (list [0 2] [1 0] [1 3] [0 5])))
  (t/is (=  (sut/find-cycles-dfs board [5 2]) (list )))
  )

(t/deftest observable
  (t/is (= (sut/observables (list [1 3] [0 5] [0 2] [1 0]))
           [(list [1 3] [0 2]) (list [0 5] [1 0])]))
  (t/is (= (sut/observables (list [0 2] [1 0] [1 3] [0 5]))
           [(list [0 2] [1 3]) (list [1 0] [0 5])])))


(def orphan-board {0
                   {:entanglements
                    {2 {:toggle true, :turn 0, :player 0, :entangle [1 0]},
                     7 {:toggle true, :turn 1, :player 1, :entangle [3 1]}}},
                   1
                   {:entanglements
                    {0 {:toggle true, :turn 0, :player 0, :entangle [0 2]},
                     2 {:toggle true, :turn 2, :player 0, :entangle [2 0]},
                     5 {:toggle true, :turn 3, :player 1, :entangle [2 3]}}},
                   3
                   {:entanglements
                    {1 {:toggle true, :turn 1, :player 1, :entangle [0 7]}}},
                   2
                   {:entanglements
                    {0 {:toggle true, :turn 2, :player 0, :entangle [1 2]},
                     3 {:toggle true, :turn 3, :player 1, :entangle [1 5]}}}})


;; (t/deftest cycle-find-orphan
;;   (t/is (=  (sut/find-cycles-dfs orphan-board [1 2])
;;             (list [1 2] [2 0] [2 3] [1 5]))))



;; Winning: (partition 3 (map #(get-in b [% :classical :player] ) (range 9)))

(def win-vert {0 {:classical {:turn 8, :player 0}},
               1 {:classical {:turn 0, :player 0}},
               2 {:classical {:turn 1, :player 1}},
               3 {:classical {:turn 3, :player 1}},
               4 {:classical {:turn 2, :player 0}},
               5 {:classical {:turn 4, :player 0}},
               6 {:classical {:turn 7, :player 1}},
               7 {:classical {:turn 6, :player 0}},
               8 {:classical {:turn 5, :player 1}}})

(def win-hor {0 {:classical {:turn 6, :player 0}},
              1 {:classical {:turn 0, :player 0}},
              3 {:classical {:turn 1, :player 1}},
              2 {:classical {:turn 2, :player 0}},
              4 {:classical {:turn 3, :player 1}},
              6 {:classical {:turn 4, :player 0}},
              5 {:classical {:turn 5, :player 1}}})


