(defproject qttt "0.0.12-SNAPSHOT"
  :description "ClojureScript Quantum Tic Tac Toe"
  :url "https://www.buildfunthings.com"

  :license {:name "GPLv3"
            :url "http://choosealicense.com/licenses/gpl-3.0/#"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.495"]
                 [reagent "0.6.1"]
                 [re-frame "0.9.2"]
                 [re-frisk "0.4.4"]
                 [devcards "0.2.2" :exclusions [cljsjs/react] ]
                 [com.taoensso/timbre "4.8.0"]]

  :plugins [[lein-cljsbuild "1.1.4"]
            [walmartlabs/vizdeps "0.1.4"]]

  :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                    "target"
                                    "test/js"]

  :profiles {:uberjar {:aot :all
                       :omit-source true
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]}
             
             :dev [:dev-overrides
                   {:dependencies [[binaryage/devtools "0.9.2"]
                                   [com.cemerick/piggieback "0.2.1"] 
                                   [figwheel-sidecar "0.5.9"]]
                    
                    :plugins [[lein-figwheel "0.5.9"]
                              [lein-doo "0.1.7"]]}]}

  :figwheel {:css-dirs ["resources/public/css"]}

  :source-paths ["src/cljs"]
  :test-paths ["test/cljs"]
  
  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/cljs"]
                        :figwheel {:on-jsload "qttt.core/mount-root"}
                        :compiler {:main qttt.core
                                   :output-to "resources/public/js/compiled/app.js"
                                   :output-dir "resources/public/js/compiled/out"
                                   :asset-path "js/compiled/out"
                                   :source-map-timestamp true
                                   :closure-defines {
                                                     qttt.db/*version* 
                                                     ~(->> (slurp "project.clj")
                                                           (re-seq #"\".*\"")
                                                           (first))}}}
                       {:id           "devcards"
                        :source-paths ["src/cljs"]
                        :figwheel     {:devcards true}
                        :compiler     {:main                 qttt.cards
                                       :output-to            "resources/public/js/compiled/app_devcards.js"
                                       :output-dir           "resources/public/js/compiled/out_devcards"
                                       :asset-path           "js/compiled/out_devcards"
                                       :source-map-timestamp true}}
                       {:id           "min"
                        :source-paths ["src/cljs"]
                        :compiler     {:main            qttt.core
                                       :output-to       "resources/public/js/compiled/app.js"
                                       :output-dir      "resources/public/js/compiled/out-min"
                                       :optimizations   :advanced
                                       :closure-defines {goog.DEBUG false}
                                       :pretty-print    false}}
                       {:id "test"
                        :source-paths ["src/cljs" "test/cljs"]
                        :compiler {:output-to "resources/public/js/compiled/test.js"
                                   :main qttt.runner
                                   :optimizations :none}}]})
