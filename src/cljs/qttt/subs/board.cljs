(ns qttt.subs.board
  (:require [re-frame.core :as re-frame]))

;; (defn reg-sub-simple [path]
;;   (re-frame/reg-sub
;;    path
;;    (fn [db]
;;      (path db))))

;; ((reg-sub-simple :selections))

(re-frame/reg-sub
 :highlight?
 (fn [db [_ index]]
   (some #{index} (:highlights db))))


(re-frame/reg-sub
 :board
 (fn [db]
   (:board db)))

(re-frame/reg-sub
 :players
 (fn [db]
   (:players db)))

(re-frame/reg-sub
 :state
 (fn [db]
   (:state db)))

(re-frame/reg-sub
 :observables
 (fn [db]
   (:observables db)))

(re-frame/reg-sub
 :cell-meta
 (fn [db [_ cell subcell]]
   (if (nil? subcell)
     (get-in db [:board cell :classical])
     (get-in db [:board cell :entanglements subcell]))))
