(ns qttt.core
  (:require [qttt.events :as events]
            [qttt.views.board :as board]
            [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [re-frisk.core :refer [enable-re-frisk!]]))

(enable-console-print!)

(defn mount-root []
  (reagent/render [board/board]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (console.log "Initializing QTTT")
  (re-frame/dispatch-sync [:initialise-db])
  (enable-re-frisk! {:x 0 :y 0})
  (mount-root)
  )

