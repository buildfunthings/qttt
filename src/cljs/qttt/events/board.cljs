(ns qttt.events.board
  (:require [qttt.game :as game]
            [re-frame.core :as re-frame]
            [taoensso.timbre :as log]))

;;; --- public events ---
(re-frame/reg-event-db
 :switch-player
 (fn [db]
   (-> db
       (assoc :selection [])
       (update-in [:players :current] (fn [current]
                                        (if (even? (inc current))
                                          0
                                          1)))
       (update-in [:turn] inc))))

(re-frame/reg-event-fx
 :collapse
 (fn [{:keys [db]}]
   {:db db}))

(re-frame/reg-event-db
 :set-state
 (fn [db [_ new-state]]
   (assoc db :state new-state)))

(re-frame/reg-event-db
 :highlight-cells
 (fn [db [_ path]]
   (assoc db :highlights path)
   ))

(re-frame/reg-event-fx
 :winning?
 (fn [{:keys [db]}]
   (let [results (game/winning? (:board db))]
     ;; (filter #(not (nil? %)) results)
     ;; (some (zipmap [#{0} #{1}] (repeat true)) '(nil nil nil 1 nil))
     {:dispatch-n (list [:set-state :play]
                        [:switch-player])})))

(re-frame/reg-event-db
 :make-classical
 (fn [db [_ [cell subcell]]]
   (let [meta (get-in db  [:board cell :entanglements subcell])]
     (-> db
         (assoc-in [:board cell :classical] {:turn (:turn meta)
                                             :player (:player meta)})
         (update-in [:board cell] dissoc :entanglements)))))

(re-frame/reg-event-fx
 :select-path
 (fn [{:keys [db]} [_ path]]
   (let [dispatches (map (fn [e] [:make-classical e]) path)]
     {:db (dissoc db :observables)
      :dispatch-n (conj  dispatches
                         )
      :dispatch-later [{:ms 200 :dispatch [:winning?]}]
      }))
 )

(re-frame/reg-event-fx
 :should-collapse?
 (fn [{:keys [db]}]
   (let [paths (game/find-cycles-dfs (:board db) (last (:selection db)))
         opaths (game/enrich-paths (:board db) paths)
         os (game/observables opaths)]
     (if (not-empty (first os))
       {:db (-> db
                (assoc :observables os))
        :dispatch-n [[:collapse]
                     [:set-state :observe]
                     [:switch-player]]}
       {:db db
        :dispatch [:switch-player]}))))

(re-frame/reg-event-fx
 :mark-entanglements
 (fn [{:keys [db]}]
   {:db (game/mark-entanglements db)
    :dispatch [:should-collapse?]}))

(re-frame/reg-event-fx
 :valid-move?
 (fn [{:keys [db]}]
   (let [sel (:selection db)]
     (if (= 2 (count sel))
       {:dispatch [:mark-entanglements]}))
   ))

(re-frame/reg-event-fx
 :toggle-cell
 (fn [{:keys [db]} [_ cell subcell]]
   {:db (if (game/valid-cell? db cell subcell)
          (game/toggle-cell db cell subcell)
          db)
    :dispatch [:valid-move?]}
   ))

;;; :toggle-cell
;;; :valid-move?
;;; :mark-entanglements
;;; :need-to-collapse?
;;; :switch-players



;;(prn (:board @re-frame.db/app-db))


