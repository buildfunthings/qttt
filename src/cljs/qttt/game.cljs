(ns qttt.game)

(defn- valid-cell? [db cell subcell]
  (let [meta (get-in db [:board cell subcell])
        [[parent _] _] (:selection db)]
    (if (or (:toggle meta) (= parent cell))
      false
      true)))

(defn toggle-cell [db cell subcell]
  (-> db
      (assoc-in [:board cell :entanglements subcell] {:toggle true
                                                      :turn (:turn db)
                                                      :player (get-in db [:players :current])})
      (update-in [:selection] (fn [v] (conj v [cell subcell])))))

(defn mark-entanglement [db [cell subcell] target]
  (update-in db [:board cell :entanglements subcell] assoc :entangle target))

(defn mark-entanglements [db]
  (let [[f s] (:selection db)]
    (-> db
        (mark-entanglement f s)
        (mark-entanglement s f))))


(defn get-other-subcells
  "Getting other subcells in current parent excluding current-subcell"
  [cells current-subcell]
;;  (prn current-subcell " Cells "  cells)
  (filter #(not= current-subcell (first %)) cells))

(defn get-other-cells [board [cell subcell]]
  (get-other-subcells (get-in board [cell :entanglements]) subcell))

(defn path-contains-cell? [path cell]
  (some #(= cell %) path))


(defn path-visited-cell? [path cell]
  (some #(= cell (first %)) path))

(defn should-collapse? [{:keys [selection board]}]
  (let [[e [parent _]] selection]
    ;;    (cycle-entanglements board parent (into [] (reverse selection)))
    true
    false))

(defn get-entangled-pair [board [cell subcell]]
  (:entangle (get-in board [cell :entanglements subcell])))

(defn create-meta-cell [board [cell subcell]]
  (let [meta (get-in board [cell :entanglements subcell])]
    {:cell cell
     :subcell subcell
     :turn (:turn meta)}))

(defn df-search
  "Do a depth first search to find all paths leading to the `goal`.
Returns a function that searches the board for a path given a start cell"
  [board goal path visited]
;;  (prn "Path: " path " Visited: " visited)
  (let [[cell subcell] (peek path)]
    (if (= goal cell)
      path
      (let [others (get-other-cells board [cell subcell])
            cells  (remove visited (map #(vector cell (first %)) others))]
;;        (prn "Others: " others)
        (mapcat #(df-search board goal
                            (conj path  % (get-entangled-pair board %))
                            (conj visited % (get-entangled-pair board %)))
                cells)))))

(defn find-cycles-dfs [board goal]
  (let [entangle (get-entangled-pair board goal)]
    (df-search board (first goal) [goal entangle] #{goal entangle})))

;;; A user makes a move, the game goes into `:thinking` state. During
;;; this state the game will determine entanglements and possible
;;; collapses. It can either go back into an `:entangle` state or an
;;; `:collapse` state.

;;; Track the current player by the list of `players`, if the index of
;;; `current` is the same as `me` then the player can make an
;;; entanglement. Based on the index of the `current` player the color
;;; of the tile changes.

;;; When setting a mark we track the `entanglement` of the game
;;; turn. We dispatch en `entangle` event. If there is a
;;; `valid-entanglement` of 2 points, check for a possible
;;; collapse. If no collapse, move the game state to the next
;;; player.

;;; When you select 2 cells you create an entanglement so we can
;;; create a function called (entangle) to step through the game logic

;;; If an entanglement causes a cycle on the board, we need to
;;; collapse. Collapsing means that the player chooses an outcome on
;;; the board. This is done by `observing` the board by the selection
;;; of a cell.

(defn highlight-cells [board cells]
  (if (empty? cells)
    board
    (let [[cell subcell] (first cells)
          tls (rest cells)
          b   (update-in board [cell :entanglements subcell] assoc :highlight true)]
      (recur b tls))))

;; Observations
;;
;; Find the possible ways in which the board can collapse, should always be a 2
;; option thing I would think as there is a path that connects, so either you
;; choose the one way around, or the other.
;;
;; Lets try and find the possible paths given a `board`, `path` and a `chosen` cell.

(defn observables
  "Partition the list per 2s, then create a list with only the first entries 
and the other with the second entries. This seperates the 2 paths through the 
board based on the path."
  [path]
  (->> path
       (partition 2)
       ((juxt (partial map first) (partial map second)))))

(defn get-subcells-not-in-path [board path tlc]
  (filter #(not (some #{%} path)) 
          (map #(:entangle (val %))
               (get-in board [tlc :entanglements]))))

(defn add-orphans-to-path [orphans path]
  (let [o (first orphans)
        r (rest orphans)]
    (if (nil? o) ;; new : deal with nil
      path
      ;; NEW: check if TLC is already in the path
      (let [already-in-path (some? (some #{(first o)} (distinct (keys path))))
            add (if already-in-path
                  '()
                  (list o o))]
        (if (empty? r)
          (concat path add)
          (recur r (concat path add)))))))

(defn add-orphan-tlc-to-path [orphans tlc]
  (let [o (first orphans)
        r (rest orphans)]
    (if (empty? r)
      (if (nil? o) ;; NEW: deal with nil
        tlc
        (conj tlc (first o)))
      (recur r (conj tlc (first o))))))

;; add a tlc-visited list to prevent recursion into already visited TLC
(defn re-find-orphans [board tlc-list path]
  (let [cur-tlc (first tlc-list)
        rest-tlc (rest tlc-list)
        orphans (get-subcells-not-in-path board path cur-tlc)
        new-path (add-orphans-to-path orphans path)
        new-tlc (add-orphan-tlc-to-path orphans rest-tlc)
        add-tlcs (concat rest-tlc
                         (filter #(not (some #{%} (distinct (keys path)))) new-tlc))]
    (if (empty? add-tlcs)  
      new-path 
      (recur board add-tlcs new-path)
      )))

;; NEW: take the paths and add orphans
(defn enrich-paths [board paths]
  (re-find-orphans board
                   (distinct (keys paths))
                   paths))
  

(defn win? [row]
  (if (empty? (filter #(nil? %) row))
      (condp = (reduce + row)
        0 0
        3 1
        nil)
      nil))

(defn win-horizontal? [simple-board]
  (for [row simple-board]
    (win? row)))

(defn win-diag? [simple-board]
  (let [f (first (first simple-board))
        s (second (second simple-board))
        l (last (last simple-board))]
    (win? (list f s l)))
  )

(defn winning? [board]
  (let [simple (partition 3 (map #(get-in board [% :classical :player] ) (range 9)))
        win-hor (win-horizontal? simple)
        win-diag1 (win-diag? simple)
        win-diag2 (win-diag? (map reverse simple))
        ]
    (flatten (list win-hor win-diag1 win-diag2))))

