(ns qttt.events
  (:require [qttt.db :as db]
            [re-frame.core :as re-frame]))

(re-frame/reg-event-db
 :initialise-db
 (fn [cofx]
   (console.log "Initialising internal structure")
   db/default-value))
