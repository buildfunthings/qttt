(ns qttt.db
  (:require [re-frame.core :as re-frame]))

(goog-define *version* "development")

;; TODO: add schema to this
(def default-value
  {:game {:name "QTTT"
          :version *version*}

   ;; :board {
   ;;         ;; cells with an ID 0 - 9
   ;;         ;; cell contains a structure subcells 0-9
   ;;         ;;  {1 {4 {:toggle true}}}
   ;;         ;; 0 cell
   ;;         0 {:entanglements {0 {:player 0
   ;;                               :turn 0
   ;;                               :pair [1 0]
   ;;                               :highlight true}}}
   ;;         1 {:entanglements {0 {:turn 0
   ;;                               :player 0
   ;;                               :pair [0 0]}}}
   ;;         2 {:classical {:turn 1
   ;;                        :player 1}}
   ;;         3 {:classical {:turn 1
   ;;                        :player 0}}
   ;;         }
   :board {}
   ;;cell: {:entanglments {subcell - meta}
   ;;       :classical {meta}}
   :selection []
   
   :players {
             :ids [0 1]
             :current 0
             }
   :highlights []
   :turn 0
   :state :play })

;; After performing changes to the internal structure, re-initialise the DB
;; (re-frame.core/dispatch [:initialise-db])

