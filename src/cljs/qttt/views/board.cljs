(ns qttt.views.board
  (:require [qttt.events.board :as board]
            [qttt.subs.board :as subs]
            [re-frame.core :as re-frame]))

(def <sub (comp deref re-frame.core/subscribe))
(def >evt re-frame.core/dispatch)

(def svg-circle
  [:svg.icon.icon-circle-o [:use {:xlinkHref "symbol-defs.svg#icon-circle-o"}]])

(def svg-close
  [:svg.icon.icon-close [:use {:xlinkHref "symbol-defs.svg#icon-close"}]])

(defmulti create-cell :type)

(defn create-cells [board class parent cells]
  [:tr
   (for [cell cells]
     (let [meta (get-in board [parent])]
       (cond
         (nil? meta)
         ^{:key cell} [create-cell {:board board :type class :parent parent :cell cell}]
         :else ^{:key cell} [create-cell {:board board :type :open :parent parent :cell cell}]
         ))
     )])

(defn generate-cells [board parent]
  (let [meta (get-in board [parent])
        type (if (nil? parent) :superposition :open)]
    (if (not (nil? (:classical meta)))
      ^{:key parent} [create-cell {:board board :type :classical :parent parent}]
       (for [cells (partition 3 (range 9))]
         ^{:key (str cells)} [create-cells board type parent cells]))))

(defmethod create-cell :classical [{:keys [board parent]}]
  (let [data (re-frame/subscribe [:cell-meta parent])]
    [:tr
     [:td {:class (if (:player @data)
                    (str "classical " (str  "player-" (:player @data)))
                    )}
      [:span.player (condp = (:player @data)
                      0 svg-circle
                      1 svg-close
                      "")]
      [:span.turn (:turn @data)]]]))

(defmethod create-cell :superposition [{:keys [board parent cell]}]
  [:td.cell
   [:table
    [:tbody
     (generate-cells board cell)]]])

(defmethod create-cell :open [{:keys [parent cell]}]
  (let [data (re-frame/subscribe [:cell-meta parent cell])]
    [:td {:on-click #(re-frame/dispatch [:toggle-cell parent cell])
          :class (if (:player @data)
                   (str "mark " (str  "player-" (:player @data) " ")
                        (when @(re-frame/subscribe [:highlight? [parent cell]]) "highlight")
                        )
                   "open")}
     [:span.player (condp = (:player @data)
                     0 svg-circle
                     1 svg-close
                     "")]
     [:span.turn (:turn @data)]]))

(defn path-selector [path]
  ^{:key (str path)}
  [:div {:on-mouse-over #(re-frame/dispatch [:highlight-cells path])
         :on-click #(re-frame/dispatch [:select-path path])}
   (str path)])

(defn player-component [id]
  (let [current (:current @(re-frame/subscribe [:players]))]
    [:div
     [:h2 "Player " id]
     [:p (when (= current id) "Your turn!")]
     [:div
      (when (and
             (= current id)
             (= :observe @(re-frame/subscribe [:state])))
        (map #(path-selector %) @(re-frame/subscribe [:observables])))]]))

(defn instruction-component []
  (condp = @(re-frame/subscribe [:state])
    :play [:p "Select some cells"]
    :observe [:p "Other player gets to pick the observation"]
    :else [:p "What the heck?"]))

(defn board []
  [:div 
   [:div.container
    [:div.row [:div.col-md-12 [:h1.text-center "Quantum Tic Tac Toe"]]]
    [:div.row
     [:div.col-md-2
      (player-component 0)]
     [:div.col-md-8
      [:table.board
       [:tbody (generate-cells @(re-frame/subscribe [:board]) nil)]]]        
     [:div.col-md-2
      (player-component 1)]]
    [:div.row
     [:div.col-md-12
      (instruction-component)] ]]
   ])
